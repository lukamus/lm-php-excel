<?php
/**
 * Plugin Name: Easy Excel Export
 * Plugin URI: 
 * Description: Read and write Excel files
 * Version: 0.1
 * Author: Luke Morton
 * Author URI: http://lukemorton.com.au
 * License: GPL3
 * 
 * Copyright (c) 2014 Luke Morton
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @category lm-php-excel
 * @package lm-php-excel
 * @copyright Copyright (c) 2014 Luke Morton
 * @license http://www.gnu.org/licenses/gpl.php GNU General Puplic License
 */

/**
 * Load external libraries.
 */
require_once( dirname( __FILE__ ) . '/lib/PHPExcel/Classes/PHPExcel.php' );
$renderer_path = dirname( __FILE__ ) . '/lib/mpdf';
$renderer_name = PHPExcel_Settings::PDF_RENDERER_MPDF;
//$renderer_path = dirname( __FILE__ ) . '/lib/tcpdf';
//$renderer_name = PHPExcel_Settings::PDF_RENDERER_TCPDF;

/**
 * Add export buttons to WP admin pages.
 * @package lm-php-excel
 */
class LM_PHP_Excel {
    /**
     * Class variables
     */
    public $php_excel;

    /**
     * Constructor
     */
    public function __construct( $name = 'New PDF Document' ) {
        //$user = wp_get_current_user();

        $this->php_excel = new PHPExcel();
        $this->php_excel->getProperties()->setTitle( $name )
            ->setSubject( $name )
            ->setDescription( $name );

        $this->create_admin_uri();
    }

    /**
     * Export the excel file.
     * @param string $name file name to$name save document as
     */
    public function export_xls() {
        global $renderer_name, $renderer_path;

        if ( isset ( $_REQUEST['lm_action'] ) &&
            isset ( $_REQUEST['lm_cell_data'] ) &&
            isset ( $_REQUEST['lm_file'] ) ) {
            if ( $_REQUEST['lm_action'] === 'lm_export_page' ) {
                $data = json_decode( base64_decode( $_REQUEST['lm_cell_data'] ) );

                $dir = EVENT_ESPRESSO_UPLOAD_DIR . $_REQUEST['lm_file'];
                $xls = PHPExcel_IOFactory::load( $dir );
                $xls->setActiveSheetIndex( 0 );

                foreach ( $data->cells as $key => $val ) {
                    $xls->getActiveSheet()->setCellValue( $key, $val );
                }

                $this->set_header( $data->meta . '.xls', 'application/vnd.ms-excel' );
                $writer = phpexcel_iofactory::createwriter( $xls, 'Excel5' );
                $writer->save( 'php://output' );

                //$this->set_header( $name, 'application/pdf' );
                //PHPExcel_Settings::setPdfRenderer( $renderer_name, $renderer_path );
                //$writer = new PHPExcel_Writer_PDF( $xls );
                //$writer->save( 'php://output' );
            }
        }
    }

    /**
     * Create an admin page to post to as a trigger for generating the Excel file.
     */
    protected function create_admin_uri() {
        add_action( 'admin_init', array( $this, 'export_xls' ) );
    }

    /**
     * Set the header for an Excel file.
     * @param string $name file name of the excel file
     * @param string $type content type for file
     */
    private function set_header( $name, $type ) {
        header( 'Content-Type: ' . $type );
        header( "Content-Disposition: attachment; filename=\"$name\"" );
        header( 'Content-Control: max-age=0' );
    }

    /**
     * The functions below this line shouldn't be in the class.
     */
    private function get_data() {
        global $wpdb;
        $reg_id = $_REQUEST['registration_id'];

        // Constants in string substitution
        $c = function( $constant ) {
            return constant( $constant );
        };

        // Retrieve primary attendee
        $sql = "SELECT
                    *
                FROM
                    {$c('EVENTS_ATTENDEE_TABLE')}
                WHERE
                    is_primary = 1
                        AND registration_id = '%s'";
        $primary_att = $wpdb->get_results( $wpdb->prepare( $sql, $reg_id ) );
        $att_session = $primary_att[0]->attendee_session;

        // Retrieve additional attendees
        $sql = "SELECT
                    *
                FROM
                    {$c('EVENTS_ATTENDEE_TABLE')}
                WHERE
                    is_primary = 0
                        AND attendee_session = '%s'";
        $additional_att = $wpdb->get_results( $wpdb->prepare( $sql, $att_session ) );

        // Retrieve questions and answers for primary attendee
        $sql = "SELECT 
                    *
                FROM
                    {$c('EVENTS_ATTENDEE_TABLE')} AS attendee
                        JOIN
                    {$c('EVENTS_ANSWER_TABLE')} AS answer ON attendee.id = answer.attendee_id
                        JOIN
                    {$c('EVENTS_QUESTION_TABLE')} AS question ON question.id = answer.question_id
                WHERE
                    is_primary = 1
                        AND attendee.registration_id = '%s'";
        $primary_qna = $wpdb->get_results( $wpdb->prepare( $sql, $reg_id ) );

        // Cells to fill in
        $cell_additonal = array( 'E', 13 );
        $cell_primary = array( 'A', 13 );
        $cell_bracket = array( 'E', 2 );

        // Array of cell, data info
        $values = array();

        // Increment function
        $inc = function( $value ) {
            return chr( ord( $value ) + 1 );
        };

        // Decrement function
        $dec = function( $value ) {
            return chr( ord( $value ) - 1 );
        };

        // Cell data for additional attendees
        foreach ( $additional_att as $key => $value ) {
            $additional_att_str = sprintf(
                '%s %s - %s',
                $value->fname,
                $value->lname,
                $value->price_option );
            $values[implode( $cell_additonal )] = $additional_att_str;
            $cell_additonal[1]++;
        }

        // Cell data for primary attendee
        foreach ( $primary_qna as $key => $value ) {
            $values[implode( $cell_primary )] = $value->question;
            $cell_primary[0] = $inc( $cell_primary[0] );
            $values[implode( $cell_primary )] = $value->answer;
            $cell_primary[0] = $dec( $cell_primary[0] );
            $cell_primary[1]++;
        }

        //echo '<pre>';
        //print_r( $values );
        //print_r( $primary_qna );
        //print_r( $additional_att );
        //echo '</pre>';

        return $values;
    }
}

// Instantiate a singleton of this plugin
global $lm_php_excel;
$lm_php_excel = new LM_PHP_Excel();
